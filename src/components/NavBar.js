import { useContext } from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { FiShoppingCart } from "react-icons/fi";
import{Card, Dropdown, Badge, Button} from 'react-bootstrap';
import AppContext from '../AppContext'
import pic from './Images/LOGO.jpg'



export default function NavBar() {

	const {user} = useContext(AppContext);

	return(
	<>
			<Navbar bg="dark" expand="lg" className=" navMain pt-4 pb-4">

			    <Container>
			       <Link to="/">
			                     <img src={pic} alt = '' height={50} width={200} />
			                   </Link>
			        <form class="container-fluid">
			            <div class="input-group">
			              <input type="text" class="form-control me-2" aria-describedby="basic-addon1"/>
			              <button class="btn btn-outline-light" type="submit">Search</button>
			            </div>
			    	</form>
			    	<Nav>
			    	    { 
			    	     (user.id !== null) ?
			    	     <Nav.Link as= { Link } to= "/logout" className="text-light">Logout</Nav.Link>
			    	     :
			    	     <>
			    	     <Nav.Link as={ Link } to="/login" className="text-light">Login</Nav.Link>
			    	     <Nav.Link as={Link} to="/Signup" className="text-light">SignUp</Nav.Link>
			    	     </>
			    	    }	
			    	    		 
			    		<Button variant="dark">
			    				<FiShoppingCart size={30}/>	
			    		</Button>
			    	</Nav>
			    </Container>
			</Navbar>

			<Navbar bg="light" expand="lg">
	            <Container> 

	            	<Nav>
			    		<Nav.Link as={Link} to="/productview"  className= "me-2 text-dark">
			    			Products
			    		</Nav.Link>
			    	
			    		<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 1</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>
			    	
			    		<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 2</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>
			 	
			    		<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 3</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>
			    
			    		<Dropdown>
			    			<Dropdown.Toggle variant="mute">Item Category 4</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>
			    	</Nav>

	            	

	             </Container>
	    
	        </Navbar>
	</>
	)
}




