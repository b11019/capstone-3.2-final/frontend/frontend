import ReactTable from 'react-table-6';

import 'react-table-6/react-table.css';

export default function(){
	return(
		<div className="app-container">
			<table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Name_Shirt</td>
						<td>Shirt</td>
						<td>P300</td>
						<td>{true}</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}